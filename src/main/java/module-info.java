module TesteFInal {

    requires javafx.controls;
    requires java.sql;
    requires javafx.graphics;
    requires java.persistence;
    requires mysql.connector.java;
    requires javafx.fxml;
    requires javafx.base;

    opens org.example;
}