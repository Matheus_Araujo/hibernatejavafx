package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.application.Application;
import  javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main  extends  Application
{



 @Override
    public void  start(Stage primaryStage) throws Exception {
     Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
     primaryStage.setTitle(("Login do Matheus"));
     primaryStage.setScene(new Scene(root, 600, 400));
     primaryStage.show();
 }


    private static EntityManagerFactory entityManagerFactory;

    public static void main(String args[]) {

        entityManagerFactory = Persistence.createEntityManagerFactory("hibernatejpa");


        Cadastros cadastros = new Cadastros();
        cadastros.setCpf("09001444322");
        cadastros.setNome("matheus");
        cadastros.setEmail("matheus@gmail.com");
        cadastros.setSenha("23123212");


        EntityManager em = entityManagerFactory.createEntityManager();

        try {
            em.getTransaction().begin();
            em.persist(cadastros);
            em.getTransaction().commit();

        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println(("insert: " + e.getMessage()));
        } finally {
            em.close();

        }


    }


}









