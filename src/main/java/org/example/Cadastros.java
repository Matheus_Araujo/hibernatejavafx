package org.example;



import javax.persistence.*;
@Entity
public class Cadastros {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCadastro;

    @Column(nullable = false)
    private String Cpf;
    @Column(nullable = false)
    private String Email;
    @Column(nullable = false)
    private String Nome;
    @Column(nullable = false)
    private String Senha;

    public Cadastros() {
    }

    public long getIdCadastro() {
        return idCadastro;
    }

    public void setIdCadastro(long idCadastro) {
        this.idCadastro = idCadastro;
    }

    public String getCpf() {
        return Cpf;
    }

    public void setCpf(String cpf) {
        Cpf = cpf;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String senha) {
        Senha = senha;
    }

    @Override
    public String toString() {
        return "Cadastros{" +
                "idCadastro=" + idCadastro +
                ", Cpf='" + Cpf + '\'' +
                ", Email='" + Email + '\'' +
                ", Nome='" + Nome + '\'' +
                ", Senha='" + Senha + '\'' +
                '}';
    }
}