package org.example;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.event.ActionEvent;

import java.util.ResourceBundle;
import java.net.URL;


public class LoginController {

    @FXML
    private Button cancelButton;
    @FXML
    private Label loginMessageLabel;

 //   @Override
 //   public void initialize(URL,url,ResourceBundle resourceBundle){}



    public void loginButtonOnAction(ActionEvent event){
        loginMessageLabel.setText("Voce tentou logar");
    }
    public void cancelButtonOnAction(ActionEvent event){
        Stage stage = (Stage)  cancelButton.getScene().getWindow();
        stage.close();
    }

}
